<?php 
/*
Plugin Name: Metadatos de video para mis posts
Plugin URI: http://www.mcch.com
Description: Descripcion del plugin
Version: 1.0
Author: DavidFraj
Author URI: http://www.mcch.com
License: 50% para David

*/

add_action('add_meta_boxes', 'mcch_add_metabox' );

add_action('save_post', 'mcch_save_metabox');

add_action('widgets_init', 'mcch_widget_init');

function mcch_add_metabox() {
    //doc http://codex.wordpress.org/Function_Reference/add_meta_box
    add_meta_box('mcch_youtube', 'YouTube Video Link','mcch_youtube_handler', 'post');
}

function mcch_youtube_handler() {
    $value = get_post_custom($post->ID);
    $youtube_link = esc_attr($value['mcch_youtube'][0]);
    $youtube_link2 = esc_attr($value['mcch_youtube2'][0]);
    echo '<label for="mcch_youtube">YouTube Video Link</label><input type="text" id="mcch_youtube" name="mcch_youtube" value="'.$youtube_link.'" />';
    echo '<label for="mcch_youtube2">Título Video Link</label><input type="text" id="mcch_youtube2" name="mcch_youtube2" value="'.$youtube_link2.'" />';

}


/**
 * save metadata
 */
function mcch_save_metabox($post_id) {
    //don't save metadata if it's autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return; 
    }    
    
    //check if user can edit post
    if( !current_user_can( 'edit_post' ) ) {
        return;  
    }
    
    if( isset($_POST['mcch_youtube'] )) {
        update_post_meta($post_id, 'mcch_youtube',$_POST['mcch_youtube']);
    }

    if( isset($_POST['mcch_youtube2'] )) {
        update_post_meta($post_id, 'mcch_youtube2',$_POST['mcch_youtube2']);
    }
}

/**
 * register widget-> funcion para registrar el widget (llamada por add_action en la parte superior de plugin)
 * */

 function mcch_widget_init(){
 	register_widget(Mcch_Widget);
 }

class Mcch_Widget extends WP_Widget {
    function Mcch_Widget() {
        $widget_options = array(
            'classname' => 'mcch_class', //CSS
            'description' => 'Show a YouTube Video from post metadata'
        );
        
        $this->WP_Widget('mcch_id', 'YouTube Video', $widget_options);
    }
    
    /**
     * show widget form in Appearence / Widgets
     */
    function form($instance) {
        $defaults = array('title' => 'Video');
        $instance = wp_parse_args( (array) $instance, $defaults);
        
        $title = esc_attr($instance['title']);
       
        echo '<p>Title <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
    }
    
    /**
     * save widget form
     */
    function update($new_instance, $old_instance) {
        
        $instance = $old_instance;        
        $instance['title'] = strip_tags($new_instance['title']);        
        return $instance;
    }
    
    /**
     * show widget in post / page
     */
    function widget($args, $instance) {
        extract( $args );        
        $title = apply_filters('widget_title', $instance['title']);
       	
        //show only if single post
        if(is_single()) {
            echo $before_widget;
            echo $before_title.$title.$after_title;
            
            //get post metadata
            $mcch_youtube = get_post_meta(get_the_ID(), 'mcch_youtube', true);
            $mcch_youtube2 = get_post_meta(get_the_ID(), 'mcch_youtube2', true);
            
            //print widget content
            echo '<iframe width="350" height="350" frameborder="0" allowfullscreen src="http://www.youtube.com/embed/'.($mcch_youtube).'"></iframe>';  
            echo $mcch_youtube2;   
           

            
            echo $after_widget;
        }
    }
}


 ?>